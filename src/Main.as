package
{
	import feathers.utils.ScreenDensityScaleFactorManager;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import starling.core.Starling;
	import test.Game;
	
	[SWF(width = "900", height = "580", frameRate = "60", backgroundColor = "#2f2f2f")]
	
	public class Main extends Sprite
	{
		public static var instance:Main;
		public var debugShape:Shape;
		
		private var starling:Starling;
		private var scaler:ScreenDensityScaleFactorManager;
		
		public function Main()
		{
			if (this.stage)
			{
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
				this.stage.align = StageAlign.TOP_LEFT;
			}
			
			instance = this;
			debugShape = new Shape();
			debugShape.x = debugShape.y = 50;
			addChild(debugShape);
			
			this.mouseEnabled = this.mouseChildren = false;
			this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler, false, 0, true);
		}
		
		private function loaderInfo_completeHandler(event:Event):void
		{
			this.loaderInfo.removeEventListener(Event.COMPLETE, loaderInfo_completeHandler);
			
			starling = new Starling(Game, this.stage, null, null, Context3DRenderMode.AUTO, Context3DProfile.BASELINE);
			starling.skipUnchangedFrames = true;
			starling.antiAliasing = 4;
			starling.start();
			
			scaler = new ScreenDensityScaleFactorManager(starling);
			this.stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
		}
		
		private function stage_deactivateHandler(event:Event):void
		{
			starling.stop(true);
			this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event:Event):void
		{
			this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
			starling.start();
		}
	}
}