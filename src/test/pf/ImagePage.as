package test.pf
{
	import flash.display.Graphics;
	import flash.geom.Point;
	import starling.display.Image;
	import starling.display.MeshBatch;
	import starling.textures.Texture;
	
	public class ImagePage extends Image
	{
		public const BOOK_WIDTH:Number = 800;
		public const BOOK_HEIGHT:Number = 480;
		
		public const LEFT_UP_POINT:Point = new Point(0, 0);
		public const LEFT_BOTTOM_POINT:Point = new Point(0, BOOK_HEIGHT);
		public const RIGHT_UP_POINT:Point = new Point(BOOK_WIDTH, 0);
		public const RIGHT_BOTTOM_POINT:Point = new Point(BOOK_WIDTH, BOOK_HEIGHT);
		public const MID_UP_POINT:Point = new Point(BOOK_WIDTH / 2, 0);
		public const MID_BOTTOM_POINT:Point = new Point(BOOK_WIDTH / 2, BOOK_HEIGHT);
		
		private var _dragPoint:Point = new Point();
		private var _dragPointCopy:Point = new Point();
		
		private var _edgePoint:Point = new Point();
		private var _edgePointCopy:Point = new Point();
		
		private var _k1:Number = new Number(0);
		private var _k2:Number = new Number(0);
		private var _b:Number = new Number(0);
		
		/** @private */
		private var currentPoint:Point = new Point();
		private var currentPointCopy:Point = new Point();
		
		private var targetPoint:Point = new Point();
		private var targetPointCopy:Point = new Point();
		
		private var interPoint:Point = new Point();
		private var interPointCopy:Point = new Point();
		
		private var swapPoint:Point = new Point();
		
		/** @private */
		private var limitedPoint:Point = new Point();
		private var limitedPointCopy:Point = new Point();
		
		private const radius:Number = BOOK_WIDTH / 2;
		private const radiusCopy:Number = Math.sqrt(Math.pow(BOOK_HEIGHT, 2) + Math.pow(BOOK_WIDTH / 2, 2));
		
		public var softMode:Boolean;
		public var anotherTexture:Texture;
		
		/** @private */
		public function ImagePage(texture:Texture)
		{
			super(texture);
		}
		
		/** @override */
		override public function readjustSize(width:Number = -1, height:Number = -1):void
		{
			resetAllTexCoords();
			setVertexDataChanged();
			super.readjustSize();
		}
		
		protected function resetAllTexCoords():void
		{
			setTexCoords(0, 0, 0);
			setTexCoords(1, 1, 0);
			setTexCoords(2, 0, 1);
			setTexCoords(3, 1.0, 1.0);
		}
		
		public function setLocation(flipingPageLocation:Number):void
		{
			var fpl:Number = Math.abs(flipingPageLocation);
			var w:Number = BOOK_WIDTH / 2;
			var h:Number = BOOK_HEIGHT;
			var topOffset:Number = h / 8;
			
			if (flipingPageLocation >= 0)
			{
				setVertexPosition(0, w, 0);
				setVertexPosition(2, w, h);
				setVertexPosition(1, w + w * fpl, -topOffset * (1 - fpl));
				setVertexPosition(3, w + w * fpl, h + topOffset * (1 - fpl));
				
				trace("kanan 1: x: " + String(w + w * fpl) + " y: " + String(-topOffset * (1 - fpl)));
				trace("kanan 3: x: " + String(w + w * fpl) + " y: " + String(h + topOffset * (1 - fpl)));
			}
			else
			{
				setVertexPosition(1, w, 0);
				setVertexPosition(3, w, h);
				setVertexPosition(0, w - w * fpl, -topOffset * (1 - fpl));
				setVertexPosition(2, w - w * fpl, h + topOffset * (1 - fpl));
				
				trace("kiri 0: x: " + String(w - w * fpl) + " y: " + String(-topOffset * (1 - fpl)));
				trace("kiri 2: x: " + String(w - w * fpl) + " y: " + String(h + topOffset * (1 - fpl)));
			}
			
			resetAllTexCoords();
		}
		
		public function setLocationSoft(quadBatch:MeshBatch, begainPageLocationX:Number, begainPageLocationY:Number, flipingPageLocationX:Number, flipingPageLocationY:Number):void
		{
			var bx:Number = begainPageLocationX;
			var by:Number = begainPageLocationY;
			var fx:Number = flipingPageLocationX;
			var fy:Number = flipingPageLocationY;
			var w:Number = BOOK_WIDTH / 2;
			var h:Number = BOOK_HEIGHT;
			
			if (validateBegainPoint(bx, by))
			{
				currentHotType = getBegainPointType(bx, by);
				
				var mouseLocation:Point = new Point(BOOK_WIDTH / 2 + fx * BOOK_WIDTH / 2, BOOK_HEIGHT / 2 + fy * BOOK_HEIGHT / 2);
				_dragPoint.x = mouseLocation.x;
				_dragPoint.y = mouseLocation.y;
				onTurnPageByHand(mouseLocation);
				
				if (currentPointCount == 3)
				{
					if (bx > 0)
					{
						setVertexPosition(0, w, 0);
						setVertexPosition(1, w, 0);
						setTexCoords(1, 0, 0);
						setVertexPosition(2, w, h);
						setVertexPosition(3, _edgePointCopy.x, h);
						setTexCoords(3, (_edgePointCopy.x - w) / w, 1);
						quadBatch.addMesh(this);
						readjustSize();
						
						setVertexPosition(0, w, 0);
						setVertexPosition(1, 2 * w, 0);
						setVertexPosition(2, _edgePointCopy.x, h);
						setTexCoords(2, (_edgePointCopy.x - w) / w, 1);
						setVertexPosition(3, w * 2, _edgePoint.y);
						setTexCoords(3, 1, _edgePoint.y / h);
						quadBatch.addMesh(this);
						texture = anotherTexture;
						readjustSize();
						
						setVertexPosition(0, w * 2, _edgePoint.y);
						setTexCoords(0, 0, _edgePoint.y / h);
						setVertexPosition(1, w * 2, _edgePoint.y);
						setTexCoords(1, 0, _edgePoint.y / h);
						setVertexPosition(2, _dragPoint.x, _dragPoint.y);
						setVertexPosition(3, _edgePointCopy.x, h);
						setTexCoords(3, (2 * w - _edgePointCopy.x) / w, 1);
						quadBatch.addMesh(this);
					}
					else
					{
						setVertexPosition(2, 0, _edgePoint.y);
						setTexCoords(2, 0, _edgePoint.y / h);
						setVertexPosition(3, _edgePointCopy.x, h);
						setTexCoords(3, _edgePointCopy.x / w, 1);
						quadBatch.addMesh(this);
						readjustSize();
						
						setVertexPosition(0, w, 0);
						setTexCoords(0, 1, 0);
						setVertexPosition(1, w, h);
						setTexCoords(1, 1, 1);
						setVertexPosition(2, _edgePointCopy.x, h);
						setTexCoords(2, _edgePointCopy.x / w, 1);
						quadBatch.addMesh(this);
						texture = anotherTexture;
						readjustSize();
						
						setVertexPosition(0, 0, _edgePoint.y);
						setTexCoords(0, 1, _edgePoint.y / h);
						setVertexPosition(1, 0, _edgePoint.y);
						setTexCoords(1, 1, _edgePoint.y / h);
						setVertexPosition(2, _edgePointCopy.x, h);
						setTexCoords(2, (w - _edgePointCopy.x) / w, 1);
						setVertexPosition(3, _dragPoint.x, _dragPoint.y);
						quadBatch.addMesh(this);
					}
				}
				
				if (currentPointCount == 4)
				{
					if (bx > 0)
					{
						setVertexPosition(0, w, 0);
						setVertexPosition(1, _edgePoint.x, 0);
						setTexCoords(1, (_edgePoint.x - w) / w, 0);
						setVertexPosition(2, w, h);
						setVertexPosition(3, _edgePointCopy.x, h);
						setTexCoords(3, (_edgePointCopy.x - w) / w, 1);
						quadBatch.addMesh(this);
						texture = anotherTexture;
						readjustSize();
						
						setVertexPosition(0, _dragPointCopy.x, _dragPointCopy.y);
						setVertexPosition(1, _edgePoint.x, 0);
						setTexCoords(1, (2 * w - _edgePoint.x) / w, 0);
						setVertexPosition(2, _dragPoint.x, _dragPoint.y);
						setVertexPosition(3, _edgePointCopy.x, h);
						setTexCoords(3, (2 * w - _edgePointCopy.x) / w, 1);
						quadBatch.addMesh(this);
					}
					else
					{
						setVertexPosition(0, _edgePoint.x, 0);
						setTexCoords(0, _edgePoint.x / w, 0);
						setVertexPosition(2, _edgePointCopy.x, h);
						setTexCoords(2, _edgePointCopy.x / w, 1);
						quadBatch.addMesh(this);
						texture = anotherTexture;
						readjustSize();
						
						setVertexPosition(0, _edgePoint.x, 0);
						setTexCoords(0, (w - _edgePoint.x) / w, 0);
						setVertexPosition(1, _dragPointCopy.x, _dragPointCopy.y);
						setVertexPosition(2, _edgePointCopy.x, h);
						setTexCoords(2, (w - _edgePointCopy.x) / w, 1);
						setVertexPosition(3, _dragPoint.x, _dragPoint.y);
						quadBatch.addMesh(this);
					}
				}
				
				//drawPage(_dragPoint, _edgePoint, _edgePointCopy, _dragPointCopy);
			}
			else
			{
				setLocation(bx >= 0 ? 1 : -1);
			}
		}
		
		private var currentHotType:String;
		
		private function onTurnPageByHand(mouseLocation:Point):void
		{
			if (mouseLocation.x >= 0 && mouseLocation.x <= BOOK_WIDTH)
			{
				_dragPoint.x += (mouseLocation.x - _dragPoint.x) * 0.4;
				_dragPoint.y += (mouseLocation.y - _dragPoint.y) * 0.4;
			}
			else
			{
				switch (currentHotType)
				{
				case (PageVerticeType.TOP_LEFT): 
					if (mouseLocation.x > BOOK_WIDTH)
					{
						_dragPoint.x += (targetPoint.x - _dragPoint.x) * 0.5;
						_dragPoint.y += (targetPoint.y - _dragPoint.y) * 0.5;
					}
					break;
				case (PageVerticeType.BOTTOM_LEFT): 
					if (mouseLocation.x > BOOK_WIDTH)
					{
						_dragPoint.x += (targetPoint.x - _dragPoint.x) * 0.5;
						_dragPoint.y += (targetPoint.y - _dragPoint.y) * 0.5;
					}
					break;
				case (PageVerticeType.TOP_RIGHT): 
					if (mouseLocation.x < 0)
					{
						_dragPoint.x += (targetPoint.x - _dragPoint.x) * 0.5;
						_dragPoint.y += (targetPoint.y - _dragPoint.y) * 0.5;
					}
					break;
				case (PageVerticeType.BOTTOM_RIGHT): 
					if (mouseLocation.x < 0)
					{
						_dragPoint.x += (targetPoint.x - _dragPoint.x) * 0.5;
						_dragPoint.y += (targetPoint.y - _dragPoint.y) * 0.5;
					}
					break;
				}
			}
			
			limitationCalculator(_dragPoint);
			_dragPointCopy.x = currentPointCopy.x;
			_dragPointCopy.y = currentPointCopy.y;
			mathematicsCalculator(_dragPoint);
			adjustPointCalculator(currentHotType);
		}
		
		private function limitationCalculator(_dragPoint:Point):void
		{
			if (_dragPoint.y > BOOK_HEIGHT - 0.1)
				_dragPoint.y = BOOK_HEIGHT - 0.1;
			
			if (_dragPoint.x <= 0.1)
				_dragPoint.x = 0.1;
			
			if (_dragPoint.x > BOOK_WIDTH - 0.1)
				_dragPoint.x = BOOK_WIDTH - 0.1;
			
			_dragPoint.x -= BOOK_WIDTH / 2;
			_dragPoint.y -= BOOK_HEIGHT / 2;
			limitedPoint.x -= BOOK_WIDTH / 2;
			limitedPoint.y -= BOOK_HEIGHT / 2;
			limitedPointCopy.x -= BOOK_WIDTH / 2;
			limitedPointCopy.y -= BOOK_HEIGHT / 2;
			
			if (currentHotType == PageVerticeType.TOP_LEFT || currentHotType == PageVerticeType.TOP_RIGHT)
			{
				if (_dragPoint.y >= Math.sqrt(Math.pow(radius, 2) - Math.pow(_dragPoint.x, 2)) + limitedPoint.y)
				{
					_dragPoint.y = Math.sqrt(Math.pow(radius, 2) - Math.pow(_dragPoint.x, 2)) + limitedPoint.y;
				}
				
				if (_dragPoint.y <= -Math.sqrt(Math.pow(radiusCopy, 2) - Math.pow(_dragPoint.x, 2)) + limitedPointCopy.y)
				{
					_dragPoint.y = -Math.sqrt(Math.pow(radiusCopy, 2) - Math.pow(_dragPoint.x, 2)) + limitedPointCopy.y;
				}
			}
			else
			{
				if (_dragPoint.y <= -Math.sqrt(Math.pow(radius, 2) - Math.pow(_dragPoint.x, 2)) + limitedPoint.y)
				{
					_dragPoint.y = -Math.sqrt(Math.pow(radius, 2) - Math.pow(_dragPoint.x, 2)) + limitedPoint.y;
				}
				
				if (_dragPoint.y >= Math.sqrt(Math.pow(radiusCopy, 2) - Math.pow(_dragPoint.x, 2)) + limitedPointCopy.y)
				{
					_dragPoint.y = Math.sqrt(Math.pow(radiusCopy, 2) - Math.pow(_dragPoint.x, 2)) + limitedPointCopy.y;
				}
			}
			
			_dragPoint.x += BOOK_WIDTH / 2;
			_dragPoint.y += BOOK_HEIGHT / 2;
			limitedPoint.x += BOOK_WIDTH / 2;
			limitedPoint.y += BOOK_HEIGHT / 2;
			limitedPointCopy.x += BOOK_WIDTH / 2;
			limitedPointCopy.y += BOOK_HEIGHT / 2;
		}
		
		private function mathematicsCalculator(_dragPoint:Point):void
		{
			interPoint = Point.interpolate(_dragPoint, currentPoint, 0.5);
			_k1 = (_dragPoint.y - currentPoint.y) / (_dragPoint.x - currentPoint.x);
			_k2 = -1 / _k1;
			
			if (Math.abs(_k2) == Infinity)
			{
				if (_k2 >= 0)
				{
					_k2 = 1000000000000000;
				}
				else
				{
					_k2 = -1000000000000000;
				}
			}
			
			_b = interPoint.y - _k2 * interPoint.x;
			_edgePoint.x = currentPoint.x;
			
			if (currentHotType == PageVerticeType.TOP_LEFT || currentHotType == PageVerticeType.BOTTOM_RIGHT)
			{
				_edgePoint.y = -Math.abs(_k2) * _edgePoint.x + _b;
			}
			
			if (currentHotType == PageVerticeType.BOTTOM_LEFT || currentHotType == PageVerticeType.TOP_RIGHT)
			{
				_edgePoint.y = Math.abs(_k2) * _edgePoint.x + _b;
			}
			
			_edgePointCopy.y = currentPoint.y;
			_edgePointCopy.x = (_edgePointCopy.y - _b) / _k2;
		}
		
		private function adjustPointCalculator(currentHotType:String):void
		{
			switch (currentHotType)
			{
			case (PageVerticeType.TOP_LEFT): 
				if (_edgePoint.y >= currentPointCopy.y)
				{
					_edgePoint.y = currentPointCopy.y;
					_edgePoint.x = (currentPointCopy.y - _b) / _k2;
					_dragPointCopy.x = 2 * (_b - (currentPointCopy.y - _k1 * currentPointCopy.x)) / (_k1 - _k2) - currentPointCopy.x;
					_dragPointCopy.y = _k1 * _dragPointCopy.x + currentPointCopy.y - _k1 * currentPointCopy.x;
				}
				break;
			case (PageVerticeType.BOTTOM_LEFT): 
				if (_edgePoint.y <= currentPointCopy.y)
				{
					_edgePoint.y = currentPointCopy.y;
					_edgePoint.x = (currentPointCopy.y - _b) / _k2;
					_dragPointCopy.x = 2 * (_b - (currentPointCopy.y - _k1 * currentPointCopy.x)) / (_k1 - _k2) - currentPointCopy.x;
					_dragPointCopy.y = _k1 * _dragPointCopy.x + currentPointCopy.y - _k1 * currentPointCopy.x;
				}
				break;
			case (PageVerticeType.TOP_RIGHT): 
				if (_edgePoint.y >= currentPointCopy.y)
				{
					_edgePoint.y = currentPointCopy.y;
					_edgePoint.x = (currentPointCopy.y - _b) / _k2;
					_dragPointCopy.x = 2 * (_b - (currentPointCopy.y - _k1 * currentPointCopy.x)) / (_k1 - _k2) - currentPointCopy.x;
					_dragPointCopy.y = _k1 * _dragPointCopy.x + currentPointCopy.y - _k1 * currentPointCopy.x;
				}
				break;
			case (PageVerticeType.BOTTOM_RIGHT): 
				if (_edgePoint.y <= currentPointCopy.y)
				{
					_edgePoint.y = currentPointCopy.y;
					_edgePoint.x = (currentPointCopy.y - _b) / _k2;
					_dragPointCopy.x = 2 * (_b - (currentPointCopy.y - _k1 * currentPointCopy.x)) / (_k1 - _k2) - currentPointCopy.x;
					_dragPointCopy.y = _k1 * _dragPointCopy.x + currentPointCopy.y - _k1 * currentPointCopy.x;
				}
				break;
			}
		}
		
		private function drawPage(point1:Point, point2:Point, point3:Point, point4:Point):void
		{
			var g:Graphics = Main.instance.debugShape.graphics;
			g.clear();
			
			if (_k1 != 0)
			{
				g.lineStyle(1, 0x000000, 0.6);
				g.moveTo(point1.x, point1.y);
				
				g.lineTo(point3.x, point3.y);
				
				g.lineTo(point2.x, point2.y);
				var fourthDot:Boolean = false;
				
				if (_dragPointCopy.x != currentPointCopy.x && _dragPointCopy.y != currentPointCopy.y)
				{
					fourthDot = true;
					g.lineTo(point4.x, point4.y);
				}
				
				g.lineTo(point1.x, point1.y);
				return;
				
				g.beginFill(0x000000, 1);
				g.drawCircle(point1.x, point1.y, 5);
				g.endFill();
				g.beginFill(0x00FF00, 1);
				g.drawCircle(point2.x, point2.y, 5);
				g.endFill();
				g.beginFill(0xFF0000, 1);
				g.drawCircle(point3.x, point3.y, 5);
				g.endFill();
				
				if (fourthDot)
				{
					g.beginFill(0x0000FF, 1);
					g.drawCircle(point4.x, point4.y, 10);
					g.endFill();
				}
			}
		}
		
		private function get currentPointCount():int
		{
			if (_dragPointCopy.x != currentPointCopy.x && _dragPointCopy.y != currentPointCopy.y)
				return 4;
			else
				return 3;
		}
		
		public function validateBegainPoint(begainPageLocationX:Number, begainPageLocationY:Number):Boolean
		{
			var bx:Number = Math.abs(begainPageLocationX);
			var by:Number = begainPageLocationY;
			
			if (bx > 0.8 && by > 0.8)
				return true;
			else
				return false;
		}
		
		public function getBegainPointType(begainPageLocationX:Number, begainPageLocationY:Number):String
		{
			var bpType:String;
			var bx:Number = begainPageLocationX;
			var by:Number = begainPageLocationY;
			
			if (bx < 0 && by < 0)
				bpType = PageVerticeType.TOP_LEFT;
			
			if (bx > 0 && by < 0)
				bpType = PageVerticeType.TOP_RIGHT;
			
			if (bx < 0 && by > 0)
				bpType = PageVerticeType.BOTTOM_LEFT;
			
			if (bx > 0 && by > 0)
				bpType = PageVerticeType.BOTTOM_RIGHT;
			
			switch (bpType)
			{
			case (PageVerticeType.TOP_LEFT): 
				currentPoint = LEFT_UP_POINT;
				currentPointCopy = LEFT_BOTTOM_POINT;
				targetPoint = RIGHT_UP_POINT;
				targetPointCopy = RIGHT_BOTTOM_POINT;
				limitedPoint = MID_UP_POINT;
				limitedPointCopy = MID_BOTTOM_POINT;
				break;
			case (PageVerticeType.BOTTOM_LEFT): 
				currentPoint = LEFT_BOTTOM_POINT;
				currentPointCopy = LEFT_UP_POINT;
				targetPoint = RIGHT_BOTTOM_POINT;
				targetPointCopy = RIGHT_UP_POINT;
				limitedPoint = MID_BOTTOM_POINT;
				limitedPointCopy = MID_UP_POINT;
				break;
			case (PageVerticeType.TOP_RIGHT): 
				currentPoint = RIGHT_UP_POINT;
				currentPointCopy = RIGHT_BOTTOM_POINT;
				targetPoint = LEFT_UP_POINT;
				targetPointCopy = LEFT_BOTTOM_POINT;
				limitedPoint = MID_UP_POINT;
				limitedPointCopy = MID_BOTTOM_POINT;
				break;
			case (PageVerticeType.BOTTOM_RIGHT): 
				currentPoint = RIGHT_BOTTOM_POINT;
				currentPointCopy = RIGHT_UP_POINT;
				targetPoint = LEFT_BOTTOM_POINT;
				targetPointCopy = LEFT_UP_POINT;
				limitedPoint = MID_BOTTOM_POINT;
				limitedPointCopy = MID_UP_POINT;
				break;
			}
			
			return bpType;
		}
	}
}

class PageVerticeType
{
	public static const TOP_LEFT:String = "topLeft";
	public static const TOP_RIGHT:String = "topRight";
	public static const BOTTOM_LEFT:String = "bottomLeft";
	public static const BOTTOM_RIGHT:String = "bottomRight";
}