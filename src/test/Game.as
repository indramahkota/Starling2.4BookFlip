package test
{
	import flash.display.Bitmap;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import test.pf.PageFlipContainer;
	import test.pf.ShadowUtil;
	
	public class Game extends Sprite
	{
		[Embed(source = "../assets/flash-pf.xml", mimeType = "application/octet-stream")]
		public static const bookXml:Class;
		[Embed(source = "../assets/flash-pf.png")]
		protected const bookImgClass:Class;
		
		private var pageFlipContainer:PageFlipContainer;
		
		public function Game()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, initGame);
		}
		
		private function initGame(event:Event):void
		{
			var bookImgs:Bitmap = new bookImgClass();
			var xml:XML = XML(new bookXml());
			
			ShadowUtil.addShadow(bookImgs, xml);
			var texture:Texture = Texture.fromBitmap(bookImgs, false);
			var atlas:TextureAtlas = new TextureAtlas(texture, xml);
			
			pageFlipContainer = new PageFlipContainer(atlas, 800, 480, 8);
			
			var scale:Number = (stage.stageHeight) / 480;
			pageFlipContainer.scaleX = pageFlipContainer.scaleY = scale;
			
			pageFlipContainer.x = (stage.stageWidth - (scale * 800)) / 2;
			pageFlipContainer.y = (stage.stageHeight - (scale * 480)) / 2;
			
			addChild(pageFlipContainer);
		}
	}
}